#!/bin/bash

# == Functions
function replace_in_file {
	if [ -z "$1" ] || [ -z "$2" ]; then
		echo "Missing arguments in 'replace_in_file'."
		return 1
	fi

	CONF_PATH="$1"
	PATTER_NECESSARY_FOR_RECOGNITION="$2"

	if [ ! -f "$CONF_PATH" ]; then
		echo "File '$CONF_PATH' does not exist. Exitting. In 'replace_in_file'"
		return 1
	fi

	sed -i "/$PATTER_NECESSARY_FOR_RECOGNITION/ {
		r $DATA_PATH
		d
	}" $CONF_PATH
	return 0
}

function generate_webpage {
	echo '=== Generating new webpage ==='

	if [ ! -f $DATA_PATH ]; then
		echo 'Could not generate page, JSON data not found!'
		exit 1
	fi

	rm -f $HTML_FINAL_DESTINATION
	cp $HTML_TEMPLATE_PATH $HTML_FINAL_DESTINATION

	replace_in_file $HTML_FINAL_DESTINATION $HTML_TEMPLATE_DATA_TAG
	
	echo 'Webpage generated! (if you didn''t see any error message ofc)'
}

function generate_data {
	which curl 2>&1 > /dev/null
	if [ "$?" = "1" ]; then
		echo 'Could not generate data, please install curl beforehand'
		return 1
	fi

	local search_terms=''
	echo 'Enter the words you wish to look for separated by "|".'
	read search_terms
	if [ -z "$search_terms" ]; then
		echo 'Next time write something'
		return 1
	fi

	curl -s "https://api.pushshift.io/reddit/comment/search?aggs=subreddit&q=$search_terms" > $DATA_PATH

	echo 'Data generated successfully!'
	return 0
}

# == Variables
DATA_PATH='assets/data.json'
HTML_FINAL_DESTINATION='index.html'
HTML_TEMPLATE_PATH='index.template.html'
HTML_TEMPLATE_DATA_TAG='GRAPH_DATA_JSON'
menu_choice=''


# == Instructions
clear
mkdir -p assets
echo '=== Main menu ==='
echo
echo '1. Generate new data'
echo '2. Generate new webpage'
read menu_choice

case "$menu_choice" in
	[1]*) generate_data;;
	[2]*) generate_webpage;;
	*) echo 'Goodbye!'
esac
